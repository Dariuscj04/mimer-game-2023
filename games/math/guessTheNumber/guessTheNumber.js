"use strict";

// Sökväg till tillgångar
const assetsPath = "../../../../assets/";

// Importera Phaser från "phaser" modulen
import phaser from "phaser";

// Skapa en ny scen klass "guessTheNumber" som ärver från Phaser.Scene
export class guessTheNumber extends Phaser.Scene {
  constructor(config) {
    // Anropa överordnad konstruktor för Phaser.Scene
    super(config);

    // Inställningar för scenen
    Phaser.Scene.call(this, { key: "guessTheNumber", active: false });

    // Variabler för spellogik
    this.gameOn = true;
    this.score = 0;
    this.scoreMsg = "Score: ";
    this.scoreText;
    this.inputText;
    this.inputNumber;
  }

  // Förinläsning av spelresurser
  preload() {}

  // Skapa spelobjekt och visa dem på skärmen
  create() {
    // Skapa och placera textobjekt för poängen
    this.scoreText = this.add.text(10, 10, this.scoreMsg + this.score, {
      fontSize: "32px",
      fill: "#FFF",
    });

    // Lyssna på tangentbordsnedtryckningar
    this.input.keyboard.on("keydown", function (event) {
      console.dir(event);
      event.preventDefault();
      return false;
    });
  }

  // Uppdatera spellogik
  update() {}

  // Hantera händelser när en siffra trycks
  pressNumberHandler(number) {}
}
