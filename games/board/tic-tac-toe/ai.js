let Board;
let mänskligSpelare ='O';
let aiSpelare = 'X';

//vinstkombinationerna
const winCombos =[
  [0, 1, 2],
  [3, 4, 5],
  [6, 7,8],
  [0, 4, 8],
  [6, 4, 2],
  [2, 5, 8],
  [1, 4, 7],
  [0, 3, 6]
];

//returnerar alla element för den valda klassens "cell". Cells-objektet är en const vilket innebär att den inte kan skrivas över.
const cells = document.querySelectorAll('.cell');
startGame();

//Funktionen för att starta spelet, den resetar spelet och döljer slutspelets meddelande. Den visar även upp symbolvalsmenyn.
function startGame() {
  document.querySelector('.endgame').style.display = "none";
  document.querySelector('.endgame .text').innerText ="";
  document.querySelector('.selectSym').style.display = "block";
  for (let i = 0; i < cells.length; i++) {
    cells[i].innerText = '';
    cells[i].style.removeProperty('background-color');
  }
}

//Imparameter till funktionen selectsym är x eller o
function selectSym(sym){
mänskligSpelare = sym;
  aiSpelare = sym==='O' ? 'X' :'O';
  Board = Array.from(Array(9).keys());
  for (let i = 0; i < cells.length; i++) {
    cells[i].addEventListener('click', turnClick, false);
  }
  if (aiSpelare === 'X') {
    turn(bestSpot(),aiSpelare);
  }
  document.querySelector('.selectSym').style.display = "none";
}



//Denna funktionen tillkallas när en spelare klickar på en "cell" i spelbrädan. Om cellen är tom så kallar den på funktionen turn () som gör att den då placerar en symbol i cellen. Den checkar sen om spelet är vunnet eller lika och om det inte är något så kallar den på turn() igen med aiSpelarens symbol.
function turnClick(square) {
  if (typeof Board[square.target.id] ==='number') {
    turn(square.target.id, mänskligSpelare);
    if (!checkWin(Board, mänskligSpelare) && !checkTie())  
      turn(bestSpot(), aiSpelare);
  }
}

//Det kommer in ett "X" eller "O" beroende på vems tur det är, om bästa draget är på id=4 så läggs en symbol på den platsen för spelaren.
function turn(squareId, player) {
  Board[squareId] = player;
  document.getElementById(squareId).innerHTML = player;
  let gameWon = checkWin(Board, player);
  if (gameWon) gameOver(gameWon);
  checkTie();
}

//Denna funktion kontrollerar och kollar om en spelare har vunnit genom att jämföra spelarnas olika drag för de olika vinst kombinationerna.
function checkWin(board, player) {
  let plays = board.reduce((a, e, i) => (e === player) ? a.concat(i) : a, []);
  let gameWon = null;
  for (let [index, win] of winCombos.entries()) {
    if (win.every(elem => plays.indexOf(elem) > -1)) {
      gameWon = {index: index, player: player};
      break;
    }
  }
  return gameWon;
}

//Denna funktionen tillkallas när matchen är slut. Den markerar vinstkombinationerna av de "celler" som har vunnit.
function gameOver(gameWon){
  for (let index of winCombos[gameWon.index]) {
    document.getElementById(index).style.backgroundColor =
      gameWon.player === mänskligSpelare ? "blue" : "red";
  }
  for (let i=0; i < cells.length; i++) {
    cells[i].removeEventListener('click', turnClick, false);
  }
  declareWinner(gameWon.player === mänskligSpelare ? "Grattis, du vann!" : "Grattis, du är en loser!");
}

//Denna funktionen deklarerar vem som vinner spelet
function declareWinner(who) {
  document.querySelector(".endgame").style.display = "block";
  document.querySelector(".endgame .text").innerText = who;
}

//Checkar av de tomma rutorna
function emptySquares() {
  return Board.filter((elm, i) => i===elm);
}
 
//Denna funktionen använder sig av minmax algorithm för att få aiSpelaren att lägga bästa möjliga drag för att vinna eller spela lika.
function bestSpot(){
  return minimax(Board, aiSpelare).index;
}
 
//Denna funktionen checkar om spelet har blivit lika genom att räkna antlaet tomma celler på spelbrädan och om alla celler är fyllda så blir det lika.
function checkTie() {
  if (emptySquares().length === 0){
    for (cell of cells) {
      cell.style.backgroundColor = "yellow";
      cell.removeEventListener('click',turnClick, false);
    }
    declareWinner("Lika, försök igen!");
    return true;
  }
  return false;
}

//Denna funktion checkar vem som har vunnit och skriver ut resultat för en spelare om den har vunnit eller om ingen har vunnit.
function minimax(newBoard, player) {
  var availSpots = emptySquares(newBoard);
 
  if (checkWin(newBoard, mänskligSpelare)) {
    return {score: -10};
  } else if (checkWin(newBoard, aiSpelare)) {
    return {score: 10};
  } else if (availSpots.length === 0) {
    return {score: 0};
  }
 
 //Här uppdateras först spelbrädet så att aispelaren kan se vilka drag den kan göra. Sedan kollar den om den kan vinna på något drag. Annars kollar den av alla möjliga drag och checkar av resultaten för varje drag och bestämmer det draget med högst resultat. 
 var moves = [];
  for (let i = 0; i < availSpots.length; i ++) {
    var move = {};
    move.index = newBoard[availSpots[i]];
    newBoard[availSpots[i]] = player;
   
    if (player === aiSpelare)
      move.score = minimax(newBoard, mänskligSpelare).score;
    else
       move.score =  minimax(newBoard, aiSpelare).score;
    newBoard[availSpots[i]] = move.index;
    if ((player === aiSpelare && move.score === 10) || (player === mänskligSpelare && move.score === -10))
      return move;
    else
      moves.push(move);
  }
 

  //Denna for loop gör så att ai spelaren och spelaren alltid startar med ett resultat och sedan ändras detta resultat för nästkommande drag så att datorn kan få så mycket poäng som möjligt.
  let bestMove, bestScore;
  if (player === aiSpelare) {
    bestScore = -1000;
    for(let i = 0; i < moves.length; i++) {
      if (moves[i].score > bestScore) {
        bestScore = moves[i].score;
        bestMove = i;
      }
    }
  } else {
      bestScore = 1000;
      for(let i = 0; i < moves.length; i++) {
      if (moves[i].score < bestScore) {
        bestScore = moves[i].score;
        bestMove = i;
      }
    }
  }
  
  //denna returnar det uppdaterade resultatet för det bästa draget.
  return moves[bestMove];
}