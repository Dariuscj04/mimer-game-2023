"use strict";
const assetsPath = "../../../../assets/";
import Phaser from "phaser";

// Define the game scene class
export class FlygingMushrooms extends Phaser.Scene {

  // Constructor that initializes the scene
  constructor(config) {
    super(config);
    // Register the scene with a key and set it as active
    Phaser.Scene.call(this, { key: "FlygingMushrooms", active: false });
    // Set the initial game state and score
    this.gameOn = true;
    this.score = 0;
    this.scoreMsg = "Score: ";
    this.scoreText;
  }

  // Load the game assets
  preload() {
    this.load.spritesheet(
      "mushrooms",
      assetsPath + "icons/mushrooms-diffrent-color-128x128-7x3.png",
      { frameWidth: 128, frameHeight: 128 }
    );
  }

  // Create the game objects and text
  create() {
    this.scoreText = this.add.text(10, 10, this.scoreMsg + this.score, {
      fontSize: "32px",
      fill: "#FFF",
    });
    // Register a pointer input listener
    this.input.on(
      "gameobjectup",
      function (pointer, gameObject) {
        gameObject.emit("clicked", gameObject);
      },
      this
    );
  }

  // Update the game objects and state
  update() {
    if (this.gameOn) {
      // Generate random mushrooms at a set rate
      if (Math.random() < 0.22) {
        let mushroom = this.randomMushroom();
        let mushroom_image = this.add
          .image(mushroom.x, mushroom.y, "mushrooms", mushroom.number)
          .setScale(0.5);
        let mushroom_images = this.add.image(mushroom.number).setScale(0.5);
        mushroom_image.setInteractive();

        // Register the click handler for good or bad mushrooms
        if (mushroom.number % 7 === 4) {
          mushroom_image.on("clicked", this.clickGoodMushroomHandler, this);
        } else {
          mushroom_image.on("clicked", this.clickBadMushroomHandler, this);
        }
      }
    }
  }

  // Generate a random mushroom with a random color
  randomMushroom() {
    let mushroom = {};
    mushroom.x = Phaser.Math.Between(64, 736);
    mushroom.y = Phaser.Math.Between(64, 500);
    if (Phaser.Math.Between(0, 1) == 1) {
      mushroom.number = Phaser.Math.Between(0, 20);
    } else {
      mushroom.number = 11;
    }
    return mushroom;
  }

  // Handler for clicking on a good mushroom
  clickGoodMushroomHandler(mushroom) {
    this.score++;
    this.scoreText.setText(this.scoreMsg + this.score);
    mushroom.off("clicked", this.clickGoodMushroomHandler);
    mushroom.input.enabled = false;
    mushroom.setVisible(false);
    console.log(mushroom);
  }

  // Handler for clicking on a bad mushroom
  clickBadMushroomHandler(mushroom) {
    this.score -= 2;
    this.scoreText.setText(this.scoreMsg + this.score);
    mushroom.off("clicked", this.clickGoodMushroomHandler);
    mushroom.input.enabled = false;
    mushroom.setVisible(false);
    console.log(mushroom);
  }
}