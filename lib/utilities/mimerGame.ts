import Phaser from "phaser";
import {
  TitleScene,
  MenuScene,
  CreditsScene,
  AboutScene,
  HighScoreScene,
  ResultScene,
} from "../scenes/game";

interface MimerGame {
  title: string;
  start: string;
}

export function mimerGame(config: MimerGame) {
  const result: Phaser.Scene[] = [];

  result.push(
    new TitleScene({
      time: 1000,
      titleText: config.title,
      nextScene: "menuScene",
    })
  );

  result.push(
    new MenuScene({
      titleText: config.title,
      startScene: config.start,
    })
  );

  result.push(
    new HighScoreScene({
      titleText: "High Score!",
      nextScene: "menuScene",
    })
  );

  result.push(
    new CreditsScene({
      titleText: "Credits!",
      nextScene: "menuScene",
    })
  );

  result.push(
    new ResultScene({
      titleText: "Result!",
      nextScene: "menuScene",
    })
  );

  result.push(
    new AboutScene({
      nextScene: "menuScene",
    })
  );

  return result;
}
