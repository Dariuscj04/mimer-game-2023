"use strict";
import { builtinModules } from "module";
import Phaser from "phaser";

function nextSeed(seed: number) {
    seed = (seed * 16807) % 2147483647;
    return seed;
}

function rollWithSeed(seed: number, faces: number) {
    seed = (seed * 9301 + 49297) % 233280;
    let value = seed / 233280;

    return Math.floor(value * faces) + 1;
}

export default class Dice extends Phaser.GameObjects.Sprite {
    key: string;
    faces: number;
    start: number;
    value: number = NaN;
    seed: number;
    finish?: (value: number) => void;

    constructor(
        scene: Phaser.Scene,
        x: number,
        y: number,
        key: string,
        faces: number = 6,
        start: number = 0,
        seed: number = 1234242342
    ) {
        // TODO: Shopuld check if key is a texture in scene with rith numbers of frames

        super(scene, x, y, key, start + faces - 1);
        this.key = key;
        this.faces = faces;
        this.start = start;
        this.seed = seed;

        scene.add.existing(this);
        this.setInteractive();
        this.on("pointerdown", this.onDown, this);
        this.on("pointerup", this.onOver, this);
        this.on("pointerover", this.onOver, this);
        this.on("pointerout", this.onUp, this);
    }

    roll() {
        if (this.seed) {
            this.seed = nextSeed(this.seed);
            this.value = rollWithSeed(this.seed, this.faces);
        } else {
            this.value = Math.floor(Math.random() * this.faces) + 1;
        }
        this.setTexture(this.key, this.start + this.value - 1);
        if (this.finish) {
            this.finish(this.value);
        }
    }

    onFinish(finish: (value: number) => void) {
        this.finish = finish;
    }

    private onDown() {
        this.roll();
    }

    private onOver() {
        this.setTint(0xaaaaaa);
    }

    private onUp() {
        this.clearTint();
    }
}
