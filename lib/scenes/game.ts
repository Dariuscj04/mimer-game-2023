export { AboutScene } from "./about";
export { CreditsScene } from "./credit";
export { MenuScene } from "./menu";
export { ResultScene } from "./result";
export { TitleScene } from "./title";
export { HighScoreScene } from "./high-score";
