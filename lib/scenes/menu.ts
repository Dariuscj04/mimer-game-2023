import Phaser from "phaser";
import Button from "../objects/button";

export class MenuScene extends Phaser.Scene {
  config: any;
  left: number;
  top: number;
  color: string;

  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      config.key = "menuScene";
    }

    if (!config.startScene) {
      throw config.key + ": Missing start scene for game";
    }

    if (!config.titleText) {
      throw config.key + ": Missing text for title";
    }

    if (!config.time) {
      config.time = 1000;
    }

    super(config);
    this.config = config;
    this.left = 200;
    this.top = 225;
    this.color = "red";
  }

  preload() {
    this.load.spritesheet(
      "startButton",
      "/assets/buttons/start-100x50-3x1.png",
      {
        frameWidth: 100,
        frameHeight: 50,
      }
    );

    this.load.spritesheet(
      "aboutButton",
      "/assets/buttons/about-100x50-3x1.png",
      {
        frameWidth: 100,
        frameHeight: 50,
      }
    );

    this.load.spritesheet(
      "creditButton",
      "/assets/buttons/credit-100x50-3x1.png",
      {
        frameWidth: 100,
        frameHeight: 50,
      }
    );
    this.load.spritesheet(
      "highScoreButton",
      "/assets/buttons/high-score-100x50-3x1.png",
      { frameWidth: 100, frameHeight: 50 }
    );
  }

  create() {
    console.log("Create Menu scene with title " + this.config.titleText);
    this.add.text(this.left, 100, this.config.titleText, {
      fontFamily: "Arial",
      // @ts-ignore
      fontSize: 64,
      color: this.color,
    });

    let start = new Button(this, 175 + this.left, this.top, "startButton");

    start.on("pointerdown", this.onStart, this);

    let about = new Button(this, 175 + this.left, this.top + 55, "aboutButton");
    about.on("pointerdown", this.onAbout, this);

    let high_score = new Button(
      this,
      175 + this.left,
      this.top + 2 * 55,
      "highScoreButton"
    );
    high_score.on("pointerdown", this.onHighScore, this);

    let credit = new Button(
      this,
      175 + this.left,
      this.top + 3 * 55,
      "creditButton"
    );

    credit.on("pointerdown", this.onCredit, this);
  }

  onStart() {
    console.log("Start scene " + this.config.startScene);
    this.scene.start(this.config.startScene);
  }

  onAbout() {
    console.log("Start scene aboutScene");
    this.scene.start("aboutScene");
  }

  onHighScore() {
    console.log("Start scene highScoreScene");
    this.scene.start("highScoreScene");
  }

  onCredit() {
    console.log("Start scene crdeditScene");
    this.scene.start("creditScene");
  }
}
